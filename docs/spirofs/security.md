tl;dr: The authentication token is a huge entrance into infrastructure managed
by Salt. Treat it with the utmost care.

Storage
=======

SpiroFS does not authenticate or encrypt its storage.


Authentication & Authorization
==============================

SpiroFS uses macaroons as the basis of its authentication. While the tooling to
utilize the more advanced features, it does mean that limitations of a token are
cryptographically baked-in to the token.


Macaroon Details
----------------

SpiroFS does not use third-party caveats. Its first-party caveats are a
JSON-formatted object, where the keys are limiter names and the values are 
arguments to those limiters.

Supported is:

* `not_before`: int or float, disallows if the current Unix timestamp is less
  than the argument
* `not_after`: int or float, disallows if the current Unix timestamp is greater
  than the argument
* `project_allow`: list of strings, disallows if the current project is not in
  the argument
* `project_deny`: list of string, disallows if the current project is in the
  argument


Secret Generation
-----------------

Part of macaroons is a master secret. This is generated automatically on first
use. It is generated via a cryptographically-suitable source for the system
(via Python's `os.urandom()`). It is stored next to the Salt Master's private
key.
