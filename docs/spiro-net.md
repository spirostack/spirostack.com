spiro-network
========

A collection of network-related functions:

* Get a minion's external IP
* Set a minion's hostname

This is meant to be installed on minions.

Installation
============

On minions, run `pip install spiro-network`

Or, as a state:

```
{% if grains['pythonversion'][0] == 2 %}
{% set pipbin = "/usr/bin/pip2" %}
{% else %}
{% set pipbin = "/usr/bin/pip3" %}
{% endif %}

spiro-network:
  pip.installed:
    - bin_env: {{pipbin}}
```

Interface
=========

A number of things are provided:

Modules
-------

* `ipaddr.external_four`, `ipaddr.external_six`: Queries external services for
  your IP, useful if the minion is behind a NAT or other complex network

* `ipaddr.four`, `ipaddr.six`: Collates information about a minion's IP address
  from several sources. 

    * `network.ipaddrs` / `network.ipaddrs6`
    * AWS metadata if the [metadata grain](https://docs.saltstack.com/en/latest/ref/grains/all/salt.grains.metadata.html) is available
    * `ipaddr.external_four` / `ipaddr.external_six` (above)

States
------

* `hostname.is`: Sets the system hostname, takes no arguments.

!!! note
    See the `hostname(1)` manpage for information on FQDNs and other hostname
    variants.

Configuration
=============

No configuration is required. However, you need to set
`metadata_server_grains: True` for the `ipaddr` module to be able to query the
AWS metadata service.

You may find it useful to set `ipaddr.four` / `ipaddr.six` as mine_functions so
that minions can get each other's IP address. Example:

```
mine_functions:
  ipaddr.four: []
  ipaddr.six: []
```
