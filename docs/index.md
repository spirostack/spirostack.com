# SpiroStack

A suite of extensions for [Salt](https://saltstack.com/)


## [SpiroFS and spiro-deploy](spirofs/index.md)

SpiroFS is the Continuous Deployment system SaltStack has been missing.

It uses saltenvs to allow for multiple deployment environments (prod, staging,
dev), and a specialized authentication and authorization system (so you don't
have to give your CI/CD pipeline arbitrary API access).

Current status: Beta


## [spiro-network](spiro-net.md)

spiro-network is a collection of network-related utilities, currently including:

* Discovering a minion's IP address
* Setting a minion's hostname

Current status: Beta


## SpiroIPAM

SpiroIPAM is a small system to automatically assign IPs and subnets to minions
via salt pillars. Useful for staticly assigning IPs for VPNs and servers.

Current status: Early Development

!!! seealso ""
    [Project](https://gitlab.com/spirostack/spiro-ipam)

## SpiroAPI

SpiroAPI is a new RPC protocol for salt based on SSH and built to be asyncronous
first.

Current status: Alpha

!!! seealso ""
    [Project](https://gitlab.com/spirostack/spiroapi)


# Licensing

All of SpiroStack is and always will be available for free under the terms of
the [Affero General Public License v3](https://www.gnu.org/licenses/agpl-3.0.en.html).

Commercial support and licensing is available through [Lumami Software](https://lumami.biz/spirostack).
