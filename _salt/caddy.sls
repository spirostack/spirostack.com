caddy-spirostack:
  service.running:
    - name: caddy
    - reload: true

/srv/spirostack.com:
  file.recurse:
    - source: salt://_artifacts/site
    - clean: true

/etc/caddy/sites/spirostack.com:
  file.managed:
    - watch_in:
      - service: caddy-spirostack
    - require:
      - file: /srv/spirostack.com
    - contents: |
        spirostack.com {
          import logging
          root * /srv/spirostack.com
          file_server
        }

        www.spirostack.com {
          import logging
          redir https://spirostack.com{uri} 301
        }
