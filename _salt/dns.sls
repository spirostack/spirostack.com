spirostack_a:
  boto_route53.present:
    - name: spirostack.com.
    - value:
{% for ips in salt['mine.get']('statichost-*', 'ipaddr.four').values() %}
{% for ip in ips %}
        - {{ip}}
{% endfor %}
{% endfor %}
    - zone: spirostack.com.
    - record_type: A
    - ttl: 60

www_spirostack_a:
  boto_route53.present:
    - name: www.spirostack.com.
    - value:
{% for ips in salt['mine.get']('statichost-*', 'ipaddr.four').values() %}
{% for ip in ips %}
        - {{ip}}
{% endfor %}
{% endfor %}
    - zone: spirostack.com.
    - record_type: A
    - ttl: 60

spirostack_txt:
  boto_route53.present:
    - name: spirostack.com
    - value: 
      - '"google-site-verification=ExgbaUwKKjytdMb7gQ58-VrnIttd-5GPosYOV_nd5Q8"'
      - '"v=spf1 include:aspmx.googlemail.com ~all"'
      - '"keybase-site-verification=t_QR0UmuB6QmCTR52AXhoe21cXbjtnsoDKVP9eDiO9o"'
    - zone: spirostack.com.
    - record_type: TXT
    - ttl: 60

spirostack_mx:
  boto_route53.present:
    - name: spirostack.com
    - value: 
      - '1 ASPMX.L.GOOGLE.COM.'
      - '5 ALT1.ASPMX.L.GOOGLE.COM.'
      - '5 ALT2.ASPMX.L.GOOGLE.COM.'
      - '10 ASPMX2.GOOGLEMAIL.COM.'
      - '10 ASPMX3.GOOGLEMAIL.COM.'
    - zone: spirostack.com.
    - record_type: MX
    - ttl: 60
